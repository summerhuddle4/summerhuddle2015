<?php

function countStars($data)
{
	$star_count = 0;
	$parse_data = json_decode($data);
	foreach ($parse_data->metrics as $metric)
	{
		if ($metric->result == "pass")
		{
			$star_count++;
		}
	} 
	return $star_count;
}

function makeTable($data)
{
	$json_array = json_decode($data);
	$table = "<table border=1>";
	$table .= '<tr>';
	foreach ($json_array->metric[0] as $key => $value) {
		$table .=  '<th>';
		switch ($key){
			case "name":
				$table .= "Metric Name";
				break;
			case "value":
				$table .= "Measured Value";
				break;
			case "range":
				$table .= "Range";
				break;
			case "result":
				$table .= "Result";
				break;
			default:
				$table .= "";
				break;
		}
		$table .=  '</th>';
	}
	$table .=  '</tr>';

	foreach ($json_array->metric as $metric) {
		$table .=  '<tr>';
		foreach ($metric as $key => $value) {
			$table .=  '<td>';
			$table .=   $value;
			$table .=  '</td>';
			}
		$table .=  '</tr>';
	}
	$table .= "</table>";
	echo $table;
	return $table;
}

?>