// 
//                 GNU Free Documentation License
//                  Version 1.3, 3 November 2008
// 
// 
//  Copyright (C) 2000, 2001, 2002, 2007, 2008 Free Software Foundation, Inc.
//      <http://fsf.org/>
//  Everyone is permitted to copy and distribute verbatim copies
//  of this license document, but changing it is not allowed.

// 0. PREAMBLE

// The purpose of this License is to make a manual, textbook, or other
// functional and useful document "free" in the sense of freedom: to
// assure everyone the effective freedom to copy and redistribute it,
// with or without modifying it, either commercially or noncommercially.
// Secondarily, this License preserves for the author and publisher a way
// to get credit for their work, while not being considered responsible
// for modifications made by others.

// This License is a kind of "copyleft", which means that derivative
// works of the document must themselves be free in the same sense.  It
// complements the GNU General Public License, which is a copyleft
// license designed for free software.

// We have designed this License in order to use it for manuals for free
// software, because free software needs free documentation: a free
// program should come with manuals providing the same freedoms that the
// software does.  But this License is not limited to software manuals;
// it can be used for any textual work, regardless of subject matter or
// whether it is published as a printed book.  We recommend this License
// principally for works whose purpose is instruction or reference.


// 1. APPLICABILITY AND DEFINITIONS

// This License applies to any manual or other work, in any medium, that
// contains a notice placed by the copyright holder saying it can be
// distributed under the terms of this License.  Such a notice grants a
// world-wide, royalty-free license, unlimited in duration, to use that
// work under the conditions stated herein.  The "Document", below,
// refers to any such manual or work.  Any member of the public is a
// licensee, and is addressed as "you".  You accept the license if you
// copy, modify or distribute the work in a way requiring permission
// under copyright law.
// 
// A "Modified Version" of the Document means any work containing the
// Document or a portion of it, either copied verbatim, or with
// modifications and/or translated into another language.
// 
// A "Secondary Section" is a named appendix or a front-matter section of
// the Document that deals exclusively with the relationship of the
// publishers or authors of the Document to the Document's overall
// subject (or to related matters) and contains nothing that could fall
// directly within that overall subject.  (Thus, if the Document is in
// part a textbook of mathematics, a Secondary Section may not explain
// any mathematics.)  The relationship could be a matter of historical
// connection with the subject or with related matters, or of legal,
// commercial, philosophical, ethical or political position regarding
// them.
// 
// The "Invariant Sections" are certain Secondary Sections whose titles
// are designated, as being those of Invariant Sections, in the notice
// that says that the Document is released under this License.  If a
// section does not fit the above definition of Secondary then it is not
// allowed to be designated as Invariant.  The Document may contain zero
// Invariant Sections.  If the Document does not identify any Invariant
// Sections then there are none.
// 
// The "Cover Texts" are certain short passages of text that are listed,
// as Front-Cover Texts or Back-Cover Texts, in the notice that says that
// the Document is released under this License.  A Front-Cover Text may
// be at most 5 words, and a Back-Cover Text may be at most 25 words.
// 
// A "Transparent" copy of the Document means a machine-readable copy,
// represented in a format whose specification is available to the
// general public, that is suitable for revising the document
// straightforwardly with generic text editors or (for images composed of
// pixels) generic paint programs or (for drawings) some widely available
// drawing editor, and that is suitable for input to text formatters or
// for automatic translation to a variety of formats suitable for input
// to text formatters.  A copy made in an otherwise Transparent file
// format whose markup, or absence of markup, has been arranged to thwart
// or discourage subsequent modification by readers is not Transparent.
// An image format is not Transparent if used for any substantial amount
// of text.  A copy that is not "Transparent" is called "Opaque".
// 
// Examples of suitable formats for Transparent copies include plain
// ASCII without markup, Texinfo input format, LaTeX input format, SGML
// or XML using a publicly available DTD, and standard-conforming simple
// HTML, PostScript or PDF designed for human modification.  Examples of
// transparent image formats include PNG, XCF and JPG.  Opaque formats
// include proprietary formats that can be read and edited only by
// proprietary word processors, SGML or XML for which the DTD and/or
// processing tools are not generally available, and the
// machine-generated HTML, PostScript or PDF produced by some word
// processors for output purposes only.
// 
// The "Title Page" means, for a printed book, the title page itself,
// plus such following pages as are needed to hold, legibly, the material
// this License requires to appear in the title page.  For works in
// formats which do not have any title page as such, "Title Page" means
// the text near the most prominent appearance of the work's title,
// preceding the beginning of the body of the text.
// 
// The "publisher" means any person or entity that distributes copies of
// the Document to the public.
// 
// A section "Entitled XYZ" means a named subunit of the Document whose
// title either is precisely XYZ or contains XYZ in parentheses following
// text that translates XYZ in another language.  (Here XYZ stands for a
// specific section name mentioned below, such as "Acknowledgements",
// "Dedications", "Endorsements", or "History".)  To "Preserve the Title"
// of such a section when you modify the Document means that it remains a
// section "Entitled XYZ" according to this definition.
// 
// The Document may include Warranty Disclaimers next to the notice which
// states that this License applies to the Document.  These Warranty
// Disclaimers are considered to be included by reference in this
// License, but only as regards disclaiming warranties: any other
// implication that these Warranty Disclaimers may have is void and has
// no effect on the meaning of this License.
// 
// 2. VERBATIM COPYING
// 
// You may copy and distribute the Document in any medium, either
// commercially or noncommercially, provided that this License, the
// copyright notices, and the license notice saying this License applies
// to the Document are reproduced in all copies, and that you add no
// other conditions whatsoever to those of this License.  You may not use
// technical measures to obstruct or control the reading or further
// copying of the copies you make or distribute.  However, you may accept
// compensation in exchange for copies.  If you distribute a large enough
// number of copies you must also follow the conditions in section 3.
// 
// You may also lend copies, under the same conditions stated above, and
// you may publicly display copies.
// 
// 
// 3. COPYING IN QUANTITY
// 
// If you publish printed copies (or copies in media that commonly have
// printed covers) of the Document, numbering more than 100, and the
// Document's license notice requires Cover Texts, you must enclose the
// copies in covers that carry, clearly and legibly, all these Cover
// Texts: Front-Cover Texts on the front cover, and Back-Cover Texts on 
// the back cover.  Both covers must also clearly and legibly identify 
// you as the publisher of these copies.  The front cover must present 
// the full title with all words of the title equally prominent and 
// visible.  You may add other material on the covers in addition.
// Copying with changes limited to the covers, as long as they preserve
// the title of the Document and satisfy these conditions, can be treated
// as verbatim copying in other respects.

// If the required texts for either cover are too voluminous to fit
// legibly, you should put the first ones listed (as many as fit
// reasonably) on the actual cover, and continue the rest onto adjacent
// pages.
// 
// If you publish or distribute Opaque copies of the Document numbering
// more than 100, you must either include a machine-readable Transparent
// copy along with each Opaque copy, or state in or with each Opaque copy 
// a computer-network location from which the general network-using
// public has access to download using public-standard network protocols
// a complete Transparent copy of the Document, free of added material.
// If you use the latter option, you must take reasonably prudent steps,
// when you begin distribution of Opaque copies in quantity, to ensure
// that this Transparent copy will remain thus accessible at the stated
// location until at least one year after the last time you distribute an
// Opaque copy (directly or through your agents or retailers) of that
// edition to the public.
// 
// It is requested, but not required, that you contact the authors of the
// Document well before redistributing any large number of copies, to
// give them a chance to provide you with an updated version of the
// Document.



#ifndef StdCardDeckH
#define StdCardDeckH
  
#include <string>
#include <sstream>
#include <algorithm>
  
const char HEART   = 0x03;   // May Not Work on
const char DIAMOND = 0x04;   // certain systems
const char CLUB    = 0x05;   // These are DOS standard
const char SPADE   = 0x06;   // ASCII characters
  
struct Card {
    char Suit;
    int trueVal;
    std::string faceVal;
    std::string fullName;
};
  
class Deck
{
  public:
    Deck(int DeckSize);
   ~Deck(void);
  
    void Shuffle(int passes);
    Card Deal(void);
    void Collect(void);
  
  private:
    Card *CardDeck;
    int CurrCard;
    const int DECK_SIZ;
};
  
Deck::Deck(int DeckSize = 1) : DECK_SIZ( DeckSize ) {
  std::ostringstream sConv;
  int CurrSuit, CurrDeck;
  
  CardDeck = new Card[52 * DECK_SIZ];
  for (CurrDeck = 0; CurrDeck < DECK_SIZ; CurrDeck++) {
    for (CurrSuit = 0; CurrSuit < 4; CurrSuit++) {
      for (CurrCard = 0; CurrCard < 13; CurrCard ++) {
        if ((CurrCard + 1) % 13 == 1) {
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].faceVal = 'A';
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName = "Ace of ";
           }
        else if ((CurrCard + 1) % 13 == 11) {
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].faceVal = 'J';
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName = "Jack of ";
           }
        else if ((CurrCard + 1) % 13 == 12) {
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].faceVal = 'Q';
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName = "Queen of ";
           }
        else if ((CurrCard + 1) % 13 == 0) {
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].faceVal = 'K';
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName = "King of ";
           }
        else {
           sConv << (CurrCard + 1) % 13;
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].faceVal = sConv.str();
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName = sConv.str() + " of ";
           sConv.str("");
        }
        if (CurrSuit == 0) {
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].Suit = HEART;
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName += "Hearts";
           }
        else if (CurrSuit == 1) {
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].Suit = DIAMOND;
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName += "Diamonds";
           }
        else if (CurrSuit == 2) {
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].Suit = CLUB;
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName += "Clubs";
           }
        else {
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].Suit = SPADE;
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].fullName += "Spades";
           }
        if ((CurrCard + 1) % 13 < 10)
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].trueVal = (CurrCard + 1) % 13;
        else
           CardDeck[CurrCard + (CurrSuit * 13) + (CurrDeck * 52)].trueVal =  10;
      }
    }
  }
  CurrCard = 0;
}
  
Deck::~Deck(void) {
  delete[] CardDeck;
}
  
void Deck::Shuffle(int passes = 1) {
  int currShuffle;
  for (currShuffle = 0; currShuffle < passes; currShuffle++)
      std::random_shuffle(CardDeck, CardDeck+DECK_SIZ*52);
}
  
Card Deck::Deal(void) {
  if ( CurrCard < DECK_SIZ * 52)
     return CardDeck[CurrCard++];
}
  
void Deck::Collect(void) {
   CurrCard = 0;
}
  
#endif /* StdCardDeckH */
int CPP_pro_adder(int I)
{
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;

return I;

}
int CPP_pro_adder(int I)
{
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;

return I;

}
int CPP_pro_adder(int I)
{
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;

return I;

}
int CPP_pro_adder(int I)
{
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;

return I;

}
int CPP_pro_adder(int I)
{
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;

return I;

}

int CPP_pro_adder(int I)
{
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;
I++;

return I;

}

if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){if(true){ cout << "True!" << "\n"}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}};
};
