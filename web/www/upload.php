<?php
	session_start();
?>
<html>
<head><title>Instant Feedback</title></head>
<body>
<pre>
<?php
	require_once('functions.php');
	if(isset($_POST['example_code']))
	{
		$target_file = $_POST['example_code'];
	}
	else
	{
		$target_dir = "./uploads/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);

		echo "upload successful \n";
	}
	$output = array();
	$return = 0;
	exec("./metric_calculator_main " . $target_file, $output, $return);
	$_SESSION['metric_json'] = $output[0];

	echo "Star rating " . $return . "\n";


?>
<a href = "detailed_report.php">Detailed Report</a>
</pre>
</body>
</html>
