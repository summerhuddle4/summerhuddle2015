#ifndef COMMENTS_DENSITY_H_INCLUDED
#define COMMENTS_DENSITY_H_INCLUDED

#include "metric_parser.h"
#include "metric_result.h"

class comments_density : public metric_parser, public metric_result
{
public:
    comments_density();
    virtual void parse_line(const std::string & line);
    int metric_result();

    virtual detailed_metric_result get_result();

private:
    std::size_t cumulative_total;
    std::size_t line_length;
    std::size_t comment_chars;
    int percent_comments;

};

#endif
