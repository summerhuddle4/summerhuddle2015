#ifndef DETAILED_METRIC_RESULT_H_INCLUDED
#define DETAILED_METRIC_RESULT_H_INCLUDED

#include <string>
#include <sstream>

struct detailed_metric_result
{
    std::string name;
    int value;
    std::string range;
    bool pass;

    std::string to_json();
    bool operator==(const detailed_metric_result& rhs) const;
};

#endif // DETAILED_METRIC_RESULT_HPP_INCLUDED
