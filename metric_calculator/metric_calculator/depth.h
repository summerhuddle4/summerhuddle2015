#ifndef DEPTH_INCLUDED
#define DEPTH_INCLUDED

#include "metric_result.h"
#include "metric_parser.h"
#include <vector>

struct depth_metric_type
{
    enum value {MAX , AVERAGE};
};

class depth : public metric_parser , public metric_result
{
public:
    depth(depth_metric_type::value depth_type); //This is the constructor

    virtual void parse_line(const std::string & line);
    virtual detailed_metric_result get_result();
    int metric_result();
private:
    int max_depth;
    size_t average_depth;
    int depth_count;
    int number_of_functions;
    std::vector <int> depths;
    depth_metric_type::value depth_type;

};


#endif
