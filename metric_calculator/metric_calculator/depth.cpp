#include "depth.h"
#include <string>
#include <vector>

const int max_threshold=6;
const size_t average_threshold=5;

using namespace std;

depth::depth(depth_metric_type::value depth_type) : depths(1, 0), depth_type(depth_type)
{
    depth_count=0;
    number_of_functions=0;
}

void depth::parse_line(const std::string &line )
{
    for(size_t i=0; i<line.length();i++)
    {
        if (line[i]=='{')
            depth_count++;
	    if(depths[number_of_functions]<depth_count)
		depths[number_of_functions]=depth_count;

        if (line[i]=='}')
        {
            depth_count--;

            if (depth_count==0)
            {
                number_of_functions++;
                depths.push_back (0);
            }
        }
    }
}

int depth::metric_result()
{

	max_depth=0;
	average_depth=0;
	for(size_t i=0; i<depths.size();i++)
	{
	    average_depth+=depths[i];
		if(max_depth<depths[i])
        max_depth=depths[i];
	}
    if (depth_type==depth_metric_type::MAX)
    {
        if (max_depth<max_threshold)
            return 1;
        return 0;
    }

        average_depth/=(depths.size()-1);
        if (average_depth<average_threshold)
            return 1;

        return 0;

}

detailed_metric_result depth::get_result()
{
    detailed_metric_result result;
    result.pass = metric_result();

    if(depth_type == depth_metric_type::AVERAGE)
    {
        result.name = "Average depth";
        result.value = static_cast<int>(average_depth);
        stringstream s;
        s << average_threshold;
        result.range = "< " + s.str();
    }
    else
    {
        result.name = "Max depth";
        result.value = static_cast<int>(max_depth);
        stringstream s;
        s << max_threshold;
        result.range = "< " + s.str();
    }

    return result;
}
