#include "counter.h"

using namespace std;

int counter(const std::string& line, const std::string& token)
{
    int count = 0;
    for (size_t pos = line.find(token);
        pos != string::npos;
        pos = line.find(token, ++pos))
    {
        ++count;
    }
    return count;
}
