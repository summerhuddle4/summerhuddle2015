#ifndef METRIC_PARSER_H_INCLUDED
#define METRIC_PARSER_H_INCLUDED

#include <string>

class metric_parser
{
public:
    virtual void parse_line(const std::string & line) = 0;
    virtual int metric_result() = 0;
};

#endif
